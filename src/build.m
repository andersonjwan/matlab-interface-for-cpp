root = pwd;

headers = [
    fullfile(root, 'core', 'addm.hpp')
]

includePath = [
    fullfile(root)
]

libraries = [
    fullfile(root, '..', 'libs', 'libaddm.so')
]

pkgName = 'addmlib';

% generate interface
clibgen.generateLibraryDefinition(headers,...
    'IncludePath', includePath,...
    'Libraries', libraries,...
    'PackageName', pkgName,...
    'Verbose', true);

% build interface
build(defineaddmlib);