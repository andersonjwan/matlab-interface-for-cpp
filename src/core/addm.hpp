#ifndef MATLABCPP_CORE_ADDM_H
#define MATLABCPP_CORE_ADDM_H

class AddM {
public:
  int add(int, int);
};

/* interface function */
int interfacem(int a, int b);

#endif
