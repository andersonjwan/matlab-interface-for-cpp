# MATLAB Interface for Cpp
This repository holds a simple example of how to interface with C++ through MATLAB's `clib` library.

## Run the example
This example is designed for a Linux environment. There is indeed support for alternate platforms; however, this is up to you to build the necessary runtime libraries and link properly.

To run the example, follow these steps:

1. Generate shared library (.so) from C++ source files
2. Generate MATLAB interface from C++ source files
3. Build MATLAB interface
4. Run MATLAB `addm.m` script

For step 1, run the following command in the root directory:

```bash
make
```

For step 2 and 3, run the following command in MATLAB from the `src/` directory:

```matlab
build
```

For step 4, run the following command in MATLAB from the `src/` directory:

```matlab
addm
```

**WARNING**: In Linux, before starting MATLAB, you _must_ add the shared library generated in Step 1 to the `LD_LIBRARY_PATH`. This can be done as follows:

```bash
LD_LIBRARY_PATH="<path/to/project-root/libs> matlab"
```

or, if you are using Docker, add the following flag to the `run` command:

```bash
--env LD_LIBRARY_PATH="<path/to/project-root/libs>"
```